using System.IO;
using UnityEngine;

public class Save : MonoBehaviour
{
    public MainSave save;
    private string path;

    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path = Path.Combine(Application.persistentDataPath, "Save.json");
#else 
        path = Path.Combine(Application.dataPath, "Save.json");
#endif
        if (File.Exists(path))
        {
            save = JsonUtility.FromJson<MainSave>(File.ReadAllText(path));
        }
        else
            print("not save file");
    }
    private void OnApplicationQuit()
    {
        SaveInfo();
        File.WriteAllText(path, JsonUtility.ToJson(save));
    }
    void SaveInfo()
    {
        try { save.bestScore = Game_Controller.singletone.BestScore;} catch { }
    }
    public void AllSave()
    {
        SaveInfo();
        File.WriteAllText(path, JsonUtility.ToJson(save));
    }
    public void LoadInfoSave()
    {
        Game_Controller.singletone.BestScore = save.bestScore;
    }
}
[System.Serializable]
public class MainSave
{
    public int bestScore;
}
