using UnityEngine;

public class Smoke_Particle : MonoBehaviour
{
    private ParticleSystem _particleSystem;
    private Rigidbody2D _rigidbody;
    void Start()
    {
        _particleSystem = GetComponent<ParticleSystem>();
        _rigidbody = GetComponentInParent<Rigidbody2D>();
    }
    void Update()
    {
        if(Mathf.Abs(_rigidbody.velocity.x) >= 0.2f)
        {
            _particleSystem.enableEmission = true;
        }
        else
        {
            _particleSystem.enableEmission = false;
        }
    }
}
