using UnityEngine;

public class Bonus_Turbo : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip[] sound;
    void Start()
    {
        _audioSource = FindObjectOfType<Audio_Controller>().GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            _audioSource.PlayOneShot(sound[Random.Range(0, sound.Length)]);
            try { FindObjectOfType<Car_Controller>().Turbo(); } catch { }
            Destroy(gameObject);
        }
    }
}
