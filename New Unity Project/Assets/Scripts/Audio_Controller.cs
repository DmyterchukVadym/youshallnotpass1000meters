using UnityEngine;
using UnityEngine.Audio;

public class Audio_Controller : MonoBehaviour
{
    public AudioMixerGroup mixer;
    [SerializeField] private AudioSource backgroundAudioSource;
    [SerializeField] private AudioClip[] backgroundMusics;
    [SerializeField] private AudioClip[] loading;

    private bool volumeOn = true;
    private void Start()
    {
        backgroundAudioSource.PlayOneShot(backgroundMusics[0]);
    }
    private void FixedUpdate()
    {
        if (!backgroundAudioSource.isPlaying)
        {
            backgroundAudioSource.PlayOneShot(backgroundMusics[Random.Range(0, backgroundMusics.Length)]);
        }
    }
    public void LoadingPlay()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(loading[Random.Range(0, loading.Length)]);
    }
    public void PlayMusic()
    {
        backgroundAudioSource.Stop();
        backgroundAudioSource.PlayOneShot(backgroundMusics[Random.Range(0, backgroundMusics.Length)]);
    }
    public void MusicVolume()
    {
        volumeOn = !volumeOn;
        if (volumeOn)
        {
            mixer.audioMixer.SetFloat("MusicVolume", 0);
        }
        else
            mixer.audioMixer.SetFloat("MusicVolume", -80);
    }
    public void MasterVolume()
    {
        volumeOn = !volumeOn;
        if (volumeOn)
        {
            mixer.audioMixer.SetFloat("MasterVolume", 0);
        }
        else
            mixer.audioMixer.SetFloat("MasterVolume", -80);
    }
}
