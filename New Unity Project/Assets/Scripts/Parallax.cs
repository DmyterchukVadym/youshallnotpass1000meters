using UnityEngine;

public class Parallax : MonoBehaviour
{
    Transform target;
    Material material;
    Vector2 offset;

    [SerializeField] float scale = 1.0f;
    void Start()
    {
        target = transform.root;
        material = GetComponent<SpriteRenderer>().material;
    }
    void Update()
    {
        offset = new Vector2(target.position.x / 100f/ scale, 0f);
        material.mainTextureOffset = offset;
    }
}
