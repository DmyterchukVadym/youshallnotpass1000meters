using UnityEngine;

public class Game_Controller : MonoBehaviour
{
    [SerializeField] private int maxGasoline;
    [SerializeField] private int health = 5;
    private int gasoline;
    private int score = 0;
    private int _bestScore;
    
    private Transform _transform;
    private Save save;
    private Panel_In_Game panelInGame;
    private Car_Controller car_Controller;
    private Canvas_Controller canvas_Controller;
    private static Game_Controller _singletone;
    public static Game_Controller singletone
    {
        get
        {
            return _singletone;
        }
    }
    public int Gasoline
    { get { return gasoline; } set { if (value < maxGasoline) { gasoline = value; } else { gasoline = maxGasoline; } } }
    public int Health
    { get { return health; } set { health = value; Damage(); } }
    public int BestScore
    { get { return _bestScore; } set { _bestScore = value; }}
    private void Awake()
    {
        _singletone = this;
    }
    private void Start()
    {
        gasoline = maxGasoline;
        save = FindObjectOfType<Save>();
        save.LoadInfoSave();
        _transform = transform;
        canvas_Controller = FindObjectOfType<Canvas_Controller>();
        car_Controller = FindObjectOfType<Car_Controller>();
        panelInGame = FindObjectOfType<Panel_In_Game>();
        panelInGame.SetMaxGasoline(maxGasoline);
        panelInGame.SetBestScore(_bestScore);
    }
    void OnDestroy()
    {
        try { FindObjectOfType<Camera_Move>().StopMoveCamera(true); } catch { }
        save.AllSave();
        print("save on destroy");
    }
    private void Damage()
    {
        switch (health)
        {
            case 6:
                FindObjectOfType<Fire_Particle>().DamageLevel();

                break;
            case 2:
                FindObjectOfType<Fire_Particle>().DamageLevel();
                break;
            case -1:
                canvas_Controller.GameOver();
                car_Controller.enabled = false;
                break;
            default:
                break;
        }
    }
    private void Update()
    {
        if (_transform.position.y < -15) 
        {
            canvas_Controller.GameOver();
            Destroy(gameObject);
        }
        if (Time.timeScale > 0)
        {
            if (gasoline >= 0)
            {
                gasoline -= 1;
                panelInGame.SetGasoline(gasoline);
            }
            else
            {
                canvas_Controller.GameOver();
                car_Controller.enabled = false;
            }
        }
        if ((int)_transform.position.x > score)
        {
            score = (int)_transform.position.x;
            panelInGame.SetScore(score);
        }
        if (score > _bestScore)
        { 
            _bestScore = score;
            panelInGame.SetBestScore(_bestScore);
        }
        if (Input.GetKeyDown(KeyCode.H) && health >= 0)
        {
            car_Controller.enabled = false;
            canvas_Controller.GameOver();
            health = -1;
            FindObjectOfType<Fire_Particle>().DamageLevel();
            FindObjectOfType<Fire_Particle>().DamageLevel();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            canvas_Controller.GamePause();
            car_Controller.StopMoveCar();
            car_Controller.enabled = !car_Controller.enabled;
        }
    }
}
