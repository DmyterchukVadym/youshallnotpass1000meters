using UnityEngine;

public class Camera_Move : MonoBehaviour
{
    private Transform _transform;
    private Transform _transformPlayer;
    private Vector3 _newCameraPos;
    private bool isMenu = true;
    private bool stopMove = false;

    public float dumping = 1.5f;
   
    private void Start()
    {
        _transform = transform;
        _newCameraPos = _transform.position;
    }

    private void Update()
    {
        if (!stopMove)
        {
            if (isMenu)
            {
                _newCameraPos.x = _transform.position.x + 0.01f;
                _newCameraPos.y = 0;
                _transform.position = Vector3.Lerp(_transform.position, _newCameraPos, dumping * Time.deltaTime);
            }
            else
            {
                _newCameraPos.x = _transformPlayer.position.x + 4f;
                _newCameraPos.y = 0;
                _transform.position = Vector3.Lerp(_transform.position, _newCameraPos, dumping * Time.deltaTime);
            }
        }
    }
    public void FocusInPlayer()
    {
        try
        {
            _transformPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
            if (_transformPlayer)
            {
                isMenu = false;
                StopMoveCamera(false);
            }
            else
                isMenu = true;
        }
        catch
        {
            isMenu = true;
            Debug.LogWarning("Player not found");
        }
    }
    public void StopMoveCamera(bool value)
    {
        stopMove = value;
    }
}
