using UnityEngine;

public class Button_FX : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip sound;
    private Canvas_Controller _canvas_Controller;
    private void Start()
    {
        _canvas_Controller = FindObjectOfType<Canvas_Controller>();
    }
    public void ClickSound()
    {
        _audioSource.PlayOneShot(sound);
    }
    public void LoadGame()
    {
        _canvas_Controller.ToGame();
    }
    public void LoadMenu()
    {
        _canvas_Controller.ToMenu();
    }
    public void SelectCar(int id)
    {
        _canvas_Controller.SelectCar(id);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
