using UnityEngine;

public class Fire_Particle : MonoBehaviour
{
    [SerializeField] private AudioClip[] sound;

    private AudioSource _audioSource;
    private ParticleSystem[] _particleSystems = new ParticleSystem[2];
    private int _damageLevel = 0;
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _particleSystems = GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < _particleSystems.Length; i++)
        {
            _particleSystems[i].enableEmission= false;
        }
    }
    public void DamageLevel()
    {
        _damageLevel++;
        switch (_damageLevel)
        {
            case 1:
                _audioSource.PlayOneShot(sound[0]);
                _particleSystems[1].enableEmission = true;
                break;
            case 2:
                _audioSource.Stop();
                _audioSource.PlayOneShot(sound[1]);
                _particleSystems[0].enableEmission = true;
                break;
        }
    }
}
