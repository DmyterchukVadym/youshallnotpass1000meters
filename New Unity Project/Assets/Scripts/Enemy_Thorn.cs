using UnityEngine;

public class Enemy_Thorn : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip[] sound;
    void Start()
    {
        _audioSource = FindObjectOfType<Audio_Controller>().GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            _audioSource.PlayOneShot(sound[Random.Range(0, sound.Length)]);
            Game_Controller.singletone.Health--;
            Destroy(gameObject);
        }
    }
}
