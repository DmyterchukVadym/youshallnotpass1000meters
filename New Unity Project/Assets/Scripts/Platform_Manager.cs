using UnityEngine;
using System.Linq;

public class Platform_Manager : MonoBehaviour
{
    [SerializeField] private GameObject[] decorObj;
    [SerializeField] private GameObject[] enemyObj;
    [SerializeField] private GameObject[] bonusObj;
    private int rateLacky;

    void Start()
    {
        Transform[] decor = transform.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("PointSpawnDecor")).ToArray();
        Transform[] enemy = transform.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("PointSpawnEnemy")).ToArray();
        Transform[] bonus = transform.GetComponentsInChildren<Transform>().Where(t => t.CompareTag("PointSpawnBonus")).ToArray();

        rateLacky = Random.Range(3, 6);
        for (int i = 0; i < decor.Length; i++)
        {
            if (i % rateLacky == 0) Instantiate(decorObj[Random.Range(0, decorObj.Length)], decor[i].position, Quaternion.identity, transform);
        }
        rateLacky = Random.Range(2, 4);
        for (int i = 0; i < enemy.Length; i++)
        {
            if (i % rateLacky == 0) Instantiate(enemyObj[Random.Range(0, enemyObj.Length)], enemy[i].position, Quaternion.identity, transform);
        }
        rateLacky = Random.Range(0, 3);
        if (rateLacky == 2) 
            Instantiate(bonusObj[Random.Range(0, bonusObj.Length)], bonus[Random.Range(0, bonus.Length)].position, Quaternion.identity, transform);
    }
}
