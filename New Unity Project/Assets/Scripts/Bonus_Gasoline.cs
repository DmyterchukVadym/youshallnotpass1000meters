using UnityEngine;

public class Bonus_Gasoline : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip[] sound;
    void Start()
    {
        _audioSource = FindObjectOfType<Audio_Controller>().GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            _audioSource.PlayOneShot(sound[Random.Range(0, sound.Length)]);
            Game_Controller.singletone.Gasoline += 1000;
            Destroy(gameObject);
        }
    }
}
