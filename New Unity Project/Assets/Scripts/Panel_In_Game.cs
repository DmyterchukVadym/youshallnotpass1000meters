using UnityEngine;
using UnityEngine.UI;

public class Panel_In_Game : MonoBehaviour
{
    [SerializeField] Image gasoline_icon;
    [SerializeField] private Text score;
    [SerializeField] private Text bestScore;
    [SerializeField] private Text result;

    private int _maxGasoline;
    public void SetMaxGasoline (int maxGasoline)
    {
        _maxGasoline = maxGasoline;
    }
    public void SetGasoline(int gasoline)
    {
        gasoline_icon.fillAmount = (float)gasoline / (float)_maxGasoline;
    }
    public void SetScore(int score)
    {
        this.score.text = $"Score: " + score.ToString();
    }
    public void SetBestScore(int bestScore)
    {
        this.bestScore.text = $"Best score: " + bestScore.ToString();
    }
    public void SetNewGame()
    {
        gasoline_icon.fillAmount = 1f;
        score.text = $"Score: " + 0;
        bestScore.text = $"Best score: " + Game_Controller.singletone.BestScore;
    }
    public void ShowResult()
    {
        result.text = score.text + "\n" + bestScore.text;
    }
}
