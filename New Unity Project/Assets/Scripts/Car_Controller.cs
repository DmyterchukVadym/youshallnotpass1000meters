using UnityEngine;
using System.Collections;

public class Car_Controller : MonoBehaviour
{
    private WheelJoint2D[] wheelJoints;
    private JointMotor2D frontWheel;
    private JointMotor2D backWheel;

    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    [SerializeField] private Rigidbody2D _frontWheelRigidbody;
    [SerializeField] private Transform backWheelTransform;
    private Rigidbody2D _backWheelRigidbody;

    public float maxSpeed = -1500f;
    public float maxBackSpeed = 1500f;
    public float acceleration = 750f;
    public float deacceleration = -100f;
    public float breakForce = 3000f;
    public float jumpForce = 5000f;
    private float gravity = 9.8f * Mathf.PI;
    private float angleCar = 0;
    public float radiusWhell = 0.5f;
    private int state = 3;
    
    private bool isGround;
    private bool isJump = false;
    private bool isJumpFront = false;
    public LayerMask ground;

    void Start()
    {
        _transform = transform;
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _backWheelRigidbody = backWheelTransform.GetComponent<Rigidbody2D>();
        wheelJoints = GetComponentsInChildren<WheelJoint2D>();
        frontWheel = wheelJoints[0].motor;
        backWheel = wheelJoints[1].motor;
    }
    public void StopMoveCar()
    {
        _rigidbody2D.velocity = Vector3.zero;
    }
    public void Turbo()
    {
        _rigidbody2D.AddForce(Vector2.right * 20000);
    }
    private void Update()
    {
        isGround = Physics2D.OverlapCircle(backWheelTransform.transform.position, radiusWhell, ground);
    }
    void FixedUpdate()
    {
        frontWheel.motorSpeed = backWheel.motorSpeed;
        angleCar = _transform.localEulerAngles.z;
        if (angleCar > 180) angleCar -= 360;

        if (isGround)
        {
            if (Input.GetKey(KeyCode.D))
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed -
                                       (acceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, maxSpeed, maxBackSpeed);
            }
            if (Input.GetKey(KeyCode.A))
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed +
                                       (acceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, maxSpeed, maxBackSpeed);
            }

            if (!Input.GetKey(KeyCode.D) || !Input.GetKey(KeyCode.A))
            {
                if(backWheel.motorSpeed < 0 || backWheel.motorSpeed == 0 && angleCar < 0)
                {
                    backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed -
                                       (deacceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, maxSpeed, 0);
                }
                else if(backWheel.motorSpeed > 0 || backWheel.motorSpeed == 0 && angleCar > 0)
                {
                    backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed -
                                       (-deacceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, 0, maxBackSpeed);
                }
            }
        }
        else if (!Input.GetKey(KeyCode.D) || !Input.GetKey(KeyCode.A))
        {
            if(backWheel.motorSpeed < 0)
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed - deacceleration * Time.deltaTime, maxSpeed, 0);
            }
            else if(backWheel.motorSpeed > 0)
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed + deacceleration * Time.deltaTime, 0, maxBackSpeed);
            }
        }

        if(Input.GetKey(KeyCode.D) && !isGround)
        {
            backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed -
                                       (acceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, maxSpeed, maxBackSpeed);
        }
        else if(Input.GetKey(KeyCode.A) && !isGround)
        {
            backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed +
                                       (acceleration - gravity * (angleCar / 180) * 100) * Time.deltaTime, maxSpeed, maxBackSpeed);
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if(backWheel.motorSpeed >0)
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed - breakForce * Time.deltaTime, 0, maxBackSpeed);
            }
            else if(backWheel.motorSpeed < 0)
            {
                backWheel.motorSpeed = Mathf.Clamp(backWheel.motorSpeed + breakForce * Time.deltaTime, maxSpeed, 0);
            }
        }

        if (Input.GetKey(KeyCode.W) &&!isJump && isGround )
        {
            _rigidbody2D.AddForce(Vector2.up * jumpForce);
            isJump = true;
            StartCoroutine(JumpDelay());
        }

        if (Input.GetKey(KeyCode.E) && !isJumpFront)
        {
            _frontWheelRigidbody.AddForce(Vector2.up * jumpForce/2);
            isJumpFront = true;
            StartCoroutine(JumpFrontDelay());
        }

        if (Input.GetKey(KeyCode.Q) && !isJumpFront)
        {
            _backWheelRigidbody.AddForce(Vector2.up * jumpForce / 2);
            isJumpFront = true;
            StartCoroutine(JumpFrontDelay());
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            _transform.position = new Vector3(_transform.position.x - 3f, Camera.main.transform.position.y, transform.position.z);
            _transform.eulerAngles = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.S) && isGround)
        {
            _rigidbody2D.AddForce(Vector2.down * jumpForce / 10);
        }

        wheelJoints[1].motor = backWheel;
        wheelJoints[0].motor = frontWheel;
    }
    private IEnumerator JumpDelay()
    {
        yield return new WaitForSeconds(1f);
        isJump = false;
    }
    private IEnumerator JumpFrontDelay()
    {
        yield return new WaitForSeconds(1f);
        isJumpFront = false;
    }
}


