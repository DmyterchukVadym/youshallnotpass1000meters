using System.Collections;
using UnityEngine;

public class Canvas_Controller : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Canvas canvasMainMenu;
    [SerializeField] private Canvas canvasInGame;
    [SerializeField] private Canvas canvasLoading;
    [SerializeField] private Canvas canvasPause;
    [SerializeField] private Canvas canvasScore;
    [SerializeField] private GameObject[] cars;

    private int carNumber = 0;
    private bool isPause = false;

    private GameObject carPlayer;
    private Audio_Controller audio;
    private Camera_Move camera;
    private void Start()
    {
        audio = FindObjectOfType<Audio_Controller>();
        camera = FindObjectOfType<Camera_Move>();
    }
    public void SelectCar(int carNumber)
    {
        this.carNumber = carNumber;
    }
    public void ToGame()
    {
        try { Destroy(carPlayer.gameObject); } catch { }
        Cursor.visible = false;
        canvasLoading.GetComponent<Canvas>().enabled = true;
        canvasMainMenu.GetComponent<Canvas>().enabled = false;
        canvasScore.GetComponent<Canvas>().enabled = false;
        canvasInGame.GetComponent<Canvas>().enabled = true;
        audio.LoadingPlay();
        StartCoroutine(Loading());
        carPlayer = Instantiate(cars[carNumber], Vector3.zero, Quaternion.identity, transform);
        FindObjectOfType<Panel_In_Game>().SetNewGame();
    }
    public void ToMenu()
    {
        Cursor.visible = true;
        canvasLoading.GetComponent<Canvas>().enabled = true;
        canvasMainMenu.GetComponent<Canvas>().enabled = true;
        canvasInGame.GetComponent<Canvas>().enabled = false;
        canvasScore.GetComponent<Canvas>().enabled = false;
        audio.LoadingPlay();
        StartCoroutine(Loading());
        try { Destroy(carPlayer.gameObject); } catch { }
    }
    public void GameOver()
    {
        Cursor.visible = true;
        camera.StopMoveCamera(true);
        canvasScore.GetComponent<Canvas>().enabled = true;
        FindObjectOfType<Panel_In_Game>().ShowResult();
    }
    public void GamePause()
    {
        canvasPause.GetComponent<Canvas>().enabled = !canvasPause.GetComponent<Canvas>().enabled;
        isPause = !isPause;
        Cursor.visible = !Cursor.visible;
        if (isPause)
        {
            Time.timeScale = 0;
            audio.MusicVolume();
        }
        else
        {
            Time.timeScale = 1;
            audio.MusicVolume();
        }
    }
    private IEnumerator Loading()
    {
        yield return new WaitForSeconds(2);
        camera.StopMoveCamera(false);
        camera.FocusInPlayer();
        audio.PlayMusic();
        canvasLoading.GetComponent<Canvas>().enabled = false;
    }
}
