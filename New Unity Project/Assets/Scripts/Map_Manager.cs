using UnityEngine;

public class Map_Manager : MonoBehaviour
{
    [SerializeField] private GameObject[] platforms;
    [SerializeField] private LayerMask ground;
    [SerializeField] private Transform sensor;

    private int numbNextPlatform;
    private Vector3 nextPositionPlatform;
    private GameObject nextDestroyPlatform;
    private GameObject lastPlatform;
    private GameObject nowPlatform;
    private GameObject nextPlatform;

    void Update()
    {
        if(!Physics2D.OverlapCircle(sensor.position, 5, ground))
        {
            numbNextPlatform = Random.Range(0, platforms.Length);
            switch (numbNextPlatform)
            {
                case 0:
                    nextPositionPlatform = new Vector3(Random.RandomRange(8f, 10f), Random.RandomRange(-0.99f, 1.05f), 0);
                    break;
                case 1:
                    nextPositionPlatform = new Vector3(Random.RandomRange(13f, 15f), Random.RandomRange(-0.99f, 1.05f), 0);
                    break;
                case 2:
                    nextPositionPlatform = new Vector3(Random.RandomRange(19f, 21f), Random.RandomRange(-0.99f, 1.05f), 0);
                    break;
                case 3:
                    nextPositionPlatform = new Vector3(Random.RandomRange(25f, 27f), Random.RandomRange(-0.99f, 1.05f), 0);
                    break;

            }
            GameObject temp = Instantiate(platforms[numbNextPlatform], sensor.position + nextPositionPlatform, Quaternion.identity, transform);
            
            if (!nextDestroyPlatform)
            {
                nextDestroyPlatform = temp;
            }
            else if (!lastPlatform)
            {
                lastPlatform = temp;
            }
            else if (!nowPlatform)
            {
                nowPlatform = temp;
            }
            else if (!nextPlatform)
            {
                nextPlatform = temp;
                Destroy(nextDestroyPlatform.gameObject);
                nextDestroyPlatform = lastPlatform;
                lastPlatform = nowPlatform;
                nowPlatform = nextPlatform;
                nextPlatform = null;
            }
        }
    }
}
